import json
from areas.user.model import User, load_user
from flask import Blueprint, jsonify, request
from utils.uuid_utils import validate_uuid4

__author__ = 'carlozamagni'

user_ctrl = Blueprint('user', __name__, static_folder='static', template_folder='templates')

@user_ctrl.route('/', methods=['POST'])
def new_user():
    posted_user = request.get_json(silent=True, force=True)

    if posted_user is None or posted_user is False:
        return jsonify({'error': 'payload missing or malformed'}), 400

    if posted_user.get('id') is not None and not validate_uuid4(posted_user.get('id')):
        return jsonify({'error': 'invalid user id'}), 400

    u = User(**posted_user)
    u.save()

    return jsonify(u.as_dict()), 201

@user_ctrl.route('/<string:user_id>/')
def user(user_id):
    u = load_user(user_id)

    if not u:
        return jsonify({'error': 'user not found'}), 404

    return jsonify(u.as_dict())

@user_ctrl.route('/<int:user_id>/friends')
def friends(user_id):
    return jsonify({'user_id': user_id,
                    'friends': []})

