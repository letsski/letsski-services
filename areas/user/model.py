import uuid
import storage

__author__ = 'carlozamagni'


class User(object):
    first_name = ''
    last_name = ''
    email = ''

    user_id = ''
    user_hash = ''

    def __init__(self, *args, **kwargs):
        self.first_name = kwargs.get('first_name')
        self.last_name = kwargs.get('last_name')
        self.email = kwargs.get('email')

        self.id = kwargs.get('id', kwargs.get('_id'))
        self.user_hash = kwargs.get('user_hash')

    def as_dict(self):
        d = {}
        for key in self.__dict__.keys():
            d[key] = self.__dict__[key]
        return d

    def save(self):

        if self.id is None:
            self.id = str(uuid.uuid4())
        users = storage.MongoDb.get_collection('user')
        user = self.as_dict()
        user['_id'] = user['id']
        del user['id']

        return users.save(user)


def load_user(user_id):
    users = storage.MongoDb.get_collection('user')
    u = users.find_one({'_id': str(user_id)})

    return User(**u) if u is not None else None