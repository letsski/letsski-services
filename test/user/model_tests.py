from areas.user.model import User, load_user
import storage

__author__ = 'carlozamagni'

import unittest


class UserTests(unittest.TestCase):

    def setUp(self):
        storage.MongoDb.get_collection('user').remove({'user_id': 'TEST USER 42'})

    def tearDown(self):
        storage.MongoDb.get_collection('user').remove({'user_id': 'TEST USER 42'})

    def test_creation(self):
        u1 = User(**{'first_name': 'test',
                     'last_name': 'test',
                     'email': 'test',
                     'user_id': 'TEST USER 42',
                     'user_hash': 'test'})

        self.assertIs(u1.last_name == 'test', True)
        self.assertIs(u1.user_id == 'TEST USER 42', True)

        u2 = User(**{'first_name': 'test 2',
                     'last_name': 'test 2',
                     'email': 'test 2',
                     'user_id': 'TEST USER 42 2',
                     'user_hash': 'test 2'})

        self.assertIs(u1.last_name == 'test', True)
        self.assertIs(u1.user_id == 'TEST USER 42', True)

        self.assertIs(u2.last_name == 'test 2', True)
        self.assertIs(u2.user_id == 'TEST USER 42 2', True)

    def test_dict_representation(self):
        u1 = User(**{'first_name': 'test',
                     'last_name': 'test',
                     'email': 'test',
                     'user_id': 'TEST USER 42',
                     'user_hash': 'test'})

        d = u1.as_dict()

        self.assertIs(d['email'] == 'test', True)

    def test_delete(self):
        u1 = User(**{'first_name': 'test',
                     'last_name': 'test',
                     'email': 'test',
                     'user_id': 'TEST USER 42',
                     'user_hash': 'test'})

        id = u1.save()

        self.assertIsNotNone(id)

    def test_load(self):
        u1 = User(**{'first_name': 'test',
                     'last_name': 'test',
                     'email': 'test',
                     'user_id': 'TEST USER 42',
                     'user_hash': 'test'})

        u1.save()

        u = load_user(u1.user_id)

        self.assertIs(u.user_id == u1.user_id, True)


if __name__ == '__main__':
    unittest.main()
