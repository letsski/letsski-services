from flask import Flask, jsonify
import datetime

app = Flask(__name__)

# import areas
from areas.user.controller import user_ctrl

app.register_blueprint(blueprint=user_ctrl, url_prefix='/user')


@app.route('/')
def alive():
    return jsonify({'status': 'alive',
                    'server_time': datetime.datetime.now()})


@app.errorhandler(404)
def resource_not_found(e):
    return jsonify({'error': str(e)}), 404


@app.errorhandler(500)
def page_not_found(e):
    return jsonify({'error': 'internal error'}), 500


if __name__ == '__main__':
    app.run()
