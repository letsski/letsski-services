import config
from pymongo import MongoClient

__author__ = 'carlozamagni'


mongo_db = {'dev': 'mongodb://test:test@ds031647.mongolab.com:31647/api-42',
            'prod': 'mongodb://test:test@ds031647.mongolab.com:31647/api-42'}


class MongoDb(object):

    @classmethod
    def get_collection(cls, collection_name, db_name=config.db):
        connection = MongoClient(mongo_db[config.env])
        return (connection[db_name])[collection_name]